
#include "stdafx.h"
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <vector>



struct wektor
{
	int x;
	int y;
	wektor(int x, int y) : x(x), y(y) {}

};
std::vector<wektor> pkt;
void klik(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		pkt.push_back(wektor(x, glutGet(GLUT_WINDOW_HEIGHT) - y));
	}
	glutPostRedisplay();
}





void RenderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_POINTS);
	for (int n = 0; n <pkt.size(); n++)
		glVertex2f(pkt[n].x, pkt[n].y);
	glEnd();

	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);
	for (int n = 0; n < pkt.size(); n++)
		glVertex2f(pkt[n].x, pkt[n].y);
	glEnd();

	glFlush();
}

void SetupRC(void)
{
	glPointSize(5.0f);
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
}

void ChangeSize(int w, int h)  {

	GLfloat aspectRatio;

	if (h == 0)   h = 1;

	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	aspectRatio = (GLfloat)w / (GLfloat)h;

	if (w <= h)
	{
		glOrtho(0.0f, w, 0.0f / aspectRatio, h / aspectRatio, 1.0f, -1.0f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glScalef(1.0f, (GLfloat)h / (GLfloat)w, 1.0f);
	}
	else
	{
		glOrtho(0.0 * aspectRatio, w * aspectRatio, 0.0f, h, 1.0f, -1.0f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glScalef((GLfloat)w / (GLfloat)h, 1.0f, 1.0f);
	}

}



int main(int argc, char* argv[])
{

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Points");
	glutDisplayFunc(RenderScene);
	glutReshapeFunc(ChangeSize);
	glutMouseFunc(klik);
	SetupRC();
	glutMainLoop();
	return 0;
}
